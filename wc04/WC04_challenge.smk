# -*- python -*-
import glob
import os

configfile: "config.yaml"

datadir = '/students/2019-2020/Thema11/Dataprocessing/kcduong/WC04/'
READS = [os.path.basename(x) for x in glob.glob(datadir + "*.txt")]

rule all:
    input:
        "results/out.vcf"


rule bwa_index:
    # Creates a BWA index in the genomic reference
    input:
        datadir + config["reference"]
    output:
        touch('bwa_index.done')
    shell:
        "bwa index {input}"


rule bwa_allign1:
    # Aligns the reads in the input file against the genomic reference
    input:
        check = "bwa_index.done",
        gen = datadir + config["reference"],
        reads = datadir + "{read}"
    output:
        "temp/{read}.sai"
    shell:
        "bwa aln -I -t 8 {input.gen} {input.reads} > {output}"

rule bwa_align2:
    input:
        gen = datadir + config["reference"],
        sai = 'temp/{read}.sai',
        reads = datadir + "{read}"
    output:
        "aligned/{read}.sam"
    shell:
        "bwa samse {input.gen} {input.sai} {input.reads} > {output}"


rule convert_sam_to_bam:
    # Converts the .sam file into a .bam file
    input:
        sam = 'aligned/{read}.sam'
    output:
        'temp/{read}.bam'
    shell:
        "samtools view -S -b {input.sam} > {output}"


rule sort_bam:
    # Sorts the bam file
    input:
        bam = 'temp/{read}.bam'
    output:
        'sorted/{read}.sorted.bam'
    shell:
        "samtools sort {input.bam} -o {output}"


rule detect_and_remove_duplicate:
    # Detects and remove duplicates
    input:
        bam = 'sorted/{read}.sorted.bam'
    output:
        'filtered/{read}.dedupe.bam'
    shell:
        "java -jar picard/build/libs/picard.jar MarkDuplicates \
                            MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000\
                            METRICS_FILE=out.metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            VALIDATION_STRINGENCY=LENIENT \
                            INPUT={input.bam} \
                            OUTPUT={output}"


rule index_results:
    # Indexes the results
    input:
        dedupe = 'filtered/{read}.dedupe.bam'
    output:
        touch('temp/{read}_index_results.done')
    shell:
        "samtools index {input.dedupe}"


rule create_pileup_and_bcf:
    # Creates the pileup and converts it into a .bcf file
    input:
        check = expand('temp/{read}_index_results.done', read=READS),
        gen = datadir + config["reference"],
        dedupe = expand('filtered/{read}.dedupe.bam', read=READS)
    output:
        'results/out.vcf'
    shell:
        "samtools mpileup -uf {input.gen} {input.dedupe} | bcftools view -> {output}"
