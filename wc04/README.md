# Week 4 Assignments

This week's exercises make use of external scripts, e.g. Python, R and Java.  
The required data can be found in `/commons/docent/Thema11/Dataprocessing/WC04/data/`

WC04_1 uses an R script to generate a heatmap of gene expression data of yeast.  
WC04_2 requires the installation of `Picard-tools` in the current directory. 
```
  git clone https://github.com/broadinstitute/picard.git
  cd picard/
  ./gradlew shadowJar
```
If unable to install, a cloned repository of picard can be found in `/students/2019-2020/Thema11/Dataprocessing/kcduong/WC04/`


## Built With
* Python 3.7.3
* Snakemake 5.4.3
* Java 11.0.7
* Picard 2.22.2
* R 3.5.2