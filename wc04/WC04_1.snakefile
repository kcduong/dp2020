# -*- python -*-
datadir = '/commons/docent/Thema11/Dataprocessing/WC04/data/'

rule all:
    """ final rule """
    input: 'result/heatmap.jpg'


rule make_heatmap:
    """ rule that creates heatmap from gene_ex.csv data"""
    input:
        datadir + 'gene_ex.csv'
    output:
         'result/heatmap.jpg'
    shell:
        "Rscript scriptthis.R {input} {output}"
