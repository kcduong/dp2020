#!/usr/bin/env python3

""" make_dirs.py that makes directories"""

import os

dir_name = snakemake.config["batch"]

try:
    os.makedirs("output/")
except FileExistsError:
    pass
try:
    os.makedirs("output/{}".format(dir_name))
except FileExistsError:
    pass
try:
    os.makedirs("output/{}/comparison_output".format(dir_name))
except FileExistsError:
    pass
try:
    os.makedirs("output/{}/comparison_graphs".format(dir_name))
except FileExistsError:
    pass
try:
    os.makedirs("output/{}/peptide_count".format(dir_name))
except FileExistsError:
    pass
try:
    os.makedirs("output/{}/unknown_peptides".format(dir_name))
except FileExistsError:
    pass