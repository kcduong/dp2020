# Final Asessment
## Reworking my own proteogenomics pipeline

For the final assessment I've chosen to convert an inefficient python pipeline into an optimized snakemake pipeline.  
The old pipeline consists of several short python modules, with the pipeline running them one after another.  
The function of this pipeline is to analyse and filter proteomic data. 
The data consists of lists of peptides that are the result of a tool used in an earlier step of the complete workflow.  
Different sets of tools produced different data from the same source, and this pipeline analyzes the 
differences and similarities between these data groups. 
These differences will eventually be displayed as Venn diagrams.  
Some other data will also be produced that's meant for the next steps in the complete workflow.

The steps of the pipeline are as follows:

* Comparison of sample group A and sample group B.

* Search for peptides in A and B that don't appear in a chosen protein database.

* Compare the peptides that weren't found in the database with peptides that are unique to their own group.

* Create venn diagrams of these comparisons.

* Calculate the frquency of each peptide in each sample, meant for future studies.


The DAG file `final_dag.png` displays the general overview of the pipeline. 

## Getting Started

Files for the old pipeline can be found [here](https://bitbucket.org/kcduong/proteogenomics/)  
Sample files are present in commons directory in `/students/2019-2020/Thema11/Dataprocessing/kcduong/final/data/`  
Variables in config.yaml can be changed if needed.  
The pipeline uses an extra python script that can be found in `scripts/`
The snakemake pipeline can be run with the default `snakemake` command, or you can provide the amount of cores to be used in the run
```
snakemake --cores {number}
```
Condor config files are present in case a condor run is preferred with `condor_submit condor.cfg`


## Built With
* Python 3.7.3
* Snakemake 5.4.3

And the following external libraries:
```
| Library        	| Version 	|
|-----------------	|---------	|
| BioPython       	| 1.75    	|
| matplotlib      	| 3.1.2   	|
| matplotlib-venn 	| 0.11.5  	|
| NumPy           	| 1.17.4  	|
| pandas          	| 0.25.3  	|
```