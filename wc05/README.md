# Week 5 Assignments

This week's assignments are about scheduling and logging.  
All exercises are condensed into one Snakefile.
The snakefile now uses multiple threads to speed up the processes. Use the `--cores` option to specify the amount of cpus snakemake should use.  

```
snakemake --cores 8
```
 
The speed of the runs can be measured using the benchmarking function, thats also been added to the snakefile.
some condor config files are also present, in case one wants to run it through the condor cluster.  

```
condor_submit condor.cfg
```

Logs have been added to each rule in order to note down the things that happen during each run. 
After executing the run you're able to check the updated output and log files with
```
snakemake --summary
```


## Built With
* Python 3.7.3
* Snakemake 5.4.3

No additional packages were required for this. 