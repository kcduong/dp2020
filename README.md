# Dataprocessing 2020

A Collection of carried out dataprocessing assignments.  
Assignments have been sorted in directories of their corresponding week, e.g., week 2 assignments having been sorted into 'wc02' etcetera.  
The final assessment can be found in 'final_asessment'. 


## Getting Started

(Re)running the exercises requires a virtual python3 environment and the installation of Snakemake.
 

```
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate

#install snakemake
pip3 install snakemake  
```

Some of the week exercises require certain packages but they will be specified in their own READMEs.


## Built With
* Python 3.7.3
* Snakemake 5.4.3


## Author

**Kim Chau Duong** - [kcduong](https://bitbucket.org/kcduong/)

## Acknowledgments

**Fenna Feenstra** - Providing the assignments
