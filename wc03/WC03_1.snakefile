# -*- python -*-
from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider

HTTP = HTTPRemoteProvider()

rule all:
    input:
        "test.txt"

rule bla:
    input:
      HTTP.remote("bioinf.nl/~fennaf/snakemake/test.txt")
    output:
        "test.txt"
    shell:
        "wget {input}"