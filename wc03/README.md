# Week 3 Assignments

This week's exercises are about data access.  
Due to the many exercises, the snakefiles had to be refactored into WC03_{exercise nr.}.snakefile, with WC03 being the week number.  
To run these renamed snakefiles simply use the `-s / --snakefile` option, e.g.:  

```
snakemake --snakefile WC03_3.snakefile 
```

DAG files also have these same naming conventions.

WC03_1 and WC03_2 are short snakemake pipelines that retrieve data from a remote source. It's possible that when running these files it will ask
to install certain packages.

WC03_3 uses data and has its working directory set to a commons one. 
`/students/2019-2020/Thema11/Dataprocessing/kcduong/WC03/`

WC03_4 uses the same code but is split into 2 sub files, with the main one, WC03_4_main, calling the other 2, along with a config file.  
samples can be configured in this config.yaml file, its writing style being similar to JSON.

## Built With
* Python 3.7.3
* Snakemake 5.4.3