# -*- python -*-
from os.path import join
workdir: "/students/2019-2020/Thema11/Dataprocessing/kcduong/WC03/"
FASTQ_DIR = 'data/'
MAPPED_DIR = 'mapped_reads/'
SORTED_DIR = 'sorted_reads/'
CALL_DIR = 'calls/'

rule bwa_map:
    input:
        join(FASTQ_DIR, "genome.fa"),
        join(FASTQ_DIR, "samples/{sample}.fastq"),
    output:
        join(MAPPED_DIR, "{sample}.bam")
    message:
        "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"

rule samtools_sort:
    input:
        join(MAPPED_DIR, "{sample}.bam")
    output:
        join(SORTED_DIR, "{sample}.bam")
    message:
        "executing samtools sort on the following {input} to generate the following {output}"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"

rule samtools_index:
    input:
        join(SORTED_DIR, "{sample}.bam")
    output:
        join(SORTED_DIR, "{sample}.bam.bai")
    message:
        "executing samtools index on the following {input} to generate the following {output}"
    shell:
        "samtools index {input}"
        

