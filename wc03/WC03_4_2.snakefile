# -*- python -*-
workdir: "/students/2019-2020/Thema11/Dataprocessing/kcduong/WC03/"

rule bcftools_call:
    input:
        fa=join(FASTQ_DIR, "genome.fa"),
        bam=expand(join(SORTED_DIR, "{sample}.bam"), sample=config["samples"]),
        bai=expand(join(SORTED_DIR, "{sample}.bam.bai"), sample=config["samples"])
    output:
        "calls/all.vcf"
    message:
        "aggregrating all mapped reads from the following {input.bam} and jointly bcftools call genomic variants to {output}"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"

rule report:
    input:
        join(CALL_DIR, "all.vcf")
    output:
        "out.html"
    message:
        "generating report of data from {input} in {output}"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeast reference genome
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Mr Pipeline", T1=input[0])