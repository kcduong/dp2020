# -*- python -*-
from os.path import join
SAMPLES = ["A", "B", "C"]
workdir: "/students/2019-2020/Thema11/Dataprocessing/kcduong/WC03/"
FASTQ_DIR = 'data/'
MAPPED_DIR = 'mapped_reads/'
SORTED_DIR = 'sorted_reads/'
CALL_DIR = 'calls/'


rule all:
    input:
        "out.html"

rule bwa_map:
    input:
        join(FASTQ_DIR, "genome.fa"),
        join(FASTQ_DIR, "samples/{sample}.fastq"),
    output:
        join(MAPPED_DIR, "{sample}.bam")
    message:
        "executing bwa mem on the following {input} to generate the following {output}"
    shell:
        "bwa mem {input} | samtools view -Sb - > {output}"

rule samtools_sort:
    input:
        join(MAPPED_DIR, "{sample}.bam")
    output:
        join(SORTED_DIR, "{sample}.bam")
    message:
        "executing samtools sort on the following {input} to generate the following {output}"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} "
        "-O bam {input} > {output}"

rule samtools_index:
    input:
        join(SORTED_DIR, "{sample}.bam")
    output:
        join(SORTED_DIR, "{sample}.bam.bai")
    message:
        "executing samtools index on the following {input} to generate the following {output}"
    shell:
        "samtools index {input}"

rule bcftools_call:
    input:
        fa=join(FASTQ_DIR, "genome.fa"),
        bam=expand(join(SORTED_DIR, "{sample}.bam"), sample=SAMPLES),
        bai=expand(join(SORTED_DIR, "{sample}.bam.bai"), sample=SAMPLES)
    output:
        "calls/all.vcf"
    message:
        "aggregrating all mapped reads from the following {input.bam} and jointly bcftools call genomic variants to {output}"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"

rule report:
    input:
        join(CALL_DIR, "all.vcf")
    output:
        "out.html"
    message:
        "generating report of data from {input} in {output}"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeast reference genome
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Mr Pipeline", T1=input[0])