# Week 2 Assignments

All of the exercises meant for week 2 have been condensed into one Snakefile.  
As mentioned earlier, a virtual environment and snakemake is required to run the snakefile.

It consists of a pipeline that maps sample reads to a yeast genome.  
The end result is displayed as an html file report, that notifies how many variants, compared to the reference, have been found. 

A DAG file has been provided to show the general view of the pipeline workflow.
In order to rerun the snakefile, some files might have to be deleted:
```
* out.html
* calls/
* mapped_reads/
* sorted_reads/
```

## Built With
* Python 3.7.3
* Snakemake 5.4.3

No additional packages were required for this. 